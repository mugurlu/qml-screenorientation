import QtQuick 2.2
import QtQuick.Window 2.1

Window {
    id:win
    visible: true
    width:Screen.desktopAvailableWidth
    height:Screen.desktopAvailableHeight


    Screen.onPrimaryOrientationChanged: {
        setAnchors()
    }

    function setAnchors(){
        //clear all
        lock.anchors.top = undefined
        lock.anchors.horizontalCenter =undefined
        lock.anchors.left = undefined
        lock.anchors.verticalCenter = undefined

        if(Screen.primaryOrientation === Qt.PortraitOrientation){
            lock.color ="green"
            lock.anchors.top = rect.bottom
            lock.anchors.horizontalCenter = rect.horizontalCenter
            tW.text ="W: "+ win.width
            tH.text ="H: "+ win.height
            tWs.text ="Ws: "+ Screen.width
            tHs.text ="Hs: "+ Screen.height

        }
        else if(Screen.primaryOrientation === Qt.LandscapeOrientation){
            lock.color ="blue"
            lock.anchors.left = rect.right
            lock.anchors.verticalCenter = rect.verticalCenter
            tW.text ="W: "+ win.width
            tH.text ="H: "+ win.height
            tWs.text ="Ws: "+ Screen.width
            tHs.text ="Hs: "+ Screen.height
        }
    }

    Component.onCompleted:  setAnchors()

    Rectangle {
        id:rect
        anchors.centerIn: parent
        width: 100
        height: 74
        color:"transparent"
        border.color: "#f4ba24"
        border.width: 6
    }


    Rectangle {
        id: lock
        width: 64
        height: 64
    }

    Column{
        anchors.centerIn: parent
        spacing: 10
        Text{
            id:tW//P
        }
        Text {
            id: tH//L
        }

        Text{
            id:tWs
        }
        Text {
            id: tHs
        }
    }




}
